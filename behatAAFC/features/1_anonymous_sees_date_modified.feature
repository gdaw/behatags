Feature: Date Modified Presence

  Scenario Outline: Verify that Date Modified block appears in many places
    Given I navigate to "<url>"
    Then I should see "<page-text-en>"
    And I should not see "Error message"
#    Then I should be on "<alias-en>"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "<page-text-fr>" in the "section#block-aafctheme-datemodifiedblock" element
    And I should not see "Message d'erreur"
#    Then I should be on "<alias-fr>"

    Examples:
      | url | page-text-en |page-text-fr |
      | / | Date modified | Date de modification |
      | /agricultural-programs-and-services | Date modified | Date de modification |
      | /canadas-agriculture-sectors | Date modified | Date de modification |
      | /agriculture-and-environment | Date modified | Date de modification |
      | /about-our-department/key-departmental-initiatives/food-policy | Date modified | Date de modification |
      | /agricultural-science-and-innovation | Date modified | Date de modification |
      | /international-trade | Date modified | Date de modification |
      | /youth-agriculture | Date modified | Date de modification | 
      | /indigenous-peoples-canadian-agriculture | Date modified | Date de modification |


#    Examples:
#      | url | page-text-en | alias-en | page-text-fr | alias-fr |
#      | / | Date modified |  | Date de modification | /fr |
#      | /news/newswork | Date modified | /news/newswork | Date de modification | /nouvelles/nouvelleslouvrage |
#      | /find-people-and-collaborate | Date modified | /find-people-and-collaborate | Date de modification | /fr/trouver-une-personne-et-collaborer |
#      | /branches-and-offices | Date modified | /branches-and-offices | Date de modification | /fr/directions-generales-et-bureaux |
#      | /human-resources | Date modified | /human-resources | Date de modification | /fr/ressources-humaines |
#      | /our-department | Date modified | /our-department | Date de modification | /fr/notre-ministere |
#      | /our-department/social-media-resources | Date modified | /our-department/social-media-resources | Date de modification | /fr/notre-ministere/ressources-lies-aux-medias-sociaux |
#      | /contact-us | Date modified | /contact-us | Date de modification | /fr/contactez-nous |
#      | /home/tools-and-services | Date modified | /home/tools-and-services | Date de modification | /fr/accueil/outils-et-services |
#      | /home/z-index | Date modified | /home/z-index | Date de modification | /fr/soumettre-demande-dopportunite-demploi/z-index |

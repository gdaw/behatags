#!/bin/bash

user_name=`whoami`

if ! grep -q "chromium" /etc/hosts;
then
    # code if found
    sudo chown ${user_name}: /etc/hosts
    echo "172.17.0.2 chromium" >> /etc/hosts
    sudo chown root: /etc/hosts
fi
if grep -q "chrome" /etc/hosts;
then
    # code if found
    echo `grep -Fxq "chrome" /etc/hosts`;
    git config credential.helper 'store'
else
    # code if not found
    sudo chown ${user_name}: /etc/hosts
    echo "172.17.0.2 chrome" >> /etc/hosts
    sudo chown root: /etc/hosts
    pushd /etc/apache2/sites-available;
    sudo find . -name "*.conf" -type f -exec sed -i 's/host>/host *:80>/g' {} +
    popd;
    sudo service apache2 restart;
fi
if [ ! -d ~/gitlab-runner ]; then
  mkdir ~/gitlab-runner;
fi

if [ ! -d "/tmp/debug-chrome" ]; then
  mkdir /tmp/debug-chrome
fi
if [ ! -d "/tmp/debug-phantomjs" ]; then
  mkdir /tmp/debug-phantomjs
fi
if [ ! -d "/tmp/behat-downloads" ]; then
  mkdir /tmp/behat-downloads
fi

echo "Starting docker services for behat now."
docker-compose up -d --remove-orphans

cat /etc/hosts | grep chrome
echo ""
echo "NOW VNC into 172.17.0.2 port 5900, password is 'secret'"
echo ""

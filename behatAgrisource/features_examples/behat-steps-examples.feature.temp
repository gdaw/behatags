Feature: Demonstrate Behat steps with examples
	In order to make Behat tests easier to create and update
	As a test creator
	I would benefit from having re-usable examples of Behat steps with working examples
	Note that improving the legibility and logical order of tests is easier since GIVEN or WHEN or THEN or AND or BUT are all interchangeable.

        Scenario: fast login for drupal.
                When I log in quickly with username "username" password "password1"

        Scenario: checking breadcrumbs on pdata to make sure they are structured correctly
                Given I navigate to '/'
                Then I should see "Home" in the pdata breadcrumb number "0"
                Then I should see "Procurement Data" in pdata breadcrumb number "1"
#ALTERNATIVELY YOU CAN ALSO DO THIS
                Then I should see "Home" in li element "1" inside the "ol.breadcrumbs" element
                Then I should see "Procurement Data" in li element "1" inside the "ol.breadcrumbs" element

        Scenario: checking breadcrumbs on sosaa to make sure they are structured correctly
                Given I navigate to '/my-agreements'
                Then I should see "Home" in the sosaa breadcrumb number "0"
                Then I should see "My Agreements" in the sosaa breadcrumb number "1"
#ALTERNATIVELY YOU CAN ALSO DO THIS
                Then I should see "Home" in li element "1" inside the "ol.breadcrumbs" element
                Then I should see "My Agreements" in li element "1" inside the "ol.breadcrumbs" element

	Scenario: Setting the current page at the beginning of a test, or at any stage where needed.
# Use -- GIVEN I AM ON "/INTERNAL-URL" -- to set the current page to where it should be.
		Given I am on "/my-agreements"
		Then I should see "My Agreements"
# Use -- GIVEN I AM ON "/" -- to set the current page to the website Homepage.
		Given I am on "/"
		Then I should see "My Agreements"
# Use -- GIVEN I AM ON "HTTP://EXTERNAL-URL" -- to set the current page to anything.
		Given I am on "https://buyandsell.gc.ca/"
		Then I should see "Find opportunities with Buyandsell.gc.ca/tenders"
# Can also use -- GIVEN I GO TO "TARGET-ADDRESS" -- to set the current page.
		Given I go to "https://buyandsell.gc.ca/"
		Then I should see "Find opportunities with Buyandsell.gc.ca/tenders"
# Can also use -- GIVEN I NAVIGATE TO "TARGET-ADDRESS" -- to set the current page.
		Given I navigate to "/my-agreements"
		Then I should see "My Agreements"
		
	Scenario: Confirming that specific text either is or is not on the current page.
		Given I am on "/my-agreements"
# Use -- I SHOULD SEE "SPECIFIC-TEXT" -- to confirm the text is found somewhere on the page.
		Then I should see "My Agreements"
# Use -- I SHOULD SEE "SPECIFIC-TEXT" IN THE "SPECIFIC-TARGET" ELEMENT -- to confirm the text is in the right place. The H1 title header easily confirms what page we are on.
		And I should see "My Agreements" in the "h1" element
# Use -- I SHOULD NOT SEE "SPECIFIC-TEXT" -- to confirm incorrect text is not present on the page.
		But I should not see "Username"
# Use -- I SHOULD NOT SEE "SPECIFIC-TEXT" IN THE "SPECIFIC-TARGET" ELEMENT -- to confirm incorrect text is not present in that particular element.
		And I should not see "Username" in the "h1" element
# Use -- I SHOULD SEE THE LINK "SPECIFIC-TEXT" -- to verify that a hyperlink is found with the text.
		And I should see the link "Issue a call-up"
# Use combinations of an ELEMENT with a CSS ID or CLASS to more precisely search the SPECIFIC-TARGET to be tested.
		And I should see "Issue a call-up" in the "li.menu-mlid-2156" element
		But I should not see "Username" in the "li.menu-mlid-2156" element
# Note that partial text can be searched for as well.
		And I should see "My Agree" in the "h1" element
		But I should not see "Userna"
		And I should see the link "Issue a"
# Note could also use I SHOULD SEE TEXT MATCHING "SPECIFIC-TEXT" and I SHOULD NOT SEE TEXT MATCHING "SPECIFIC-TEXT".
		
	Scenario: Clicking links or buttons and then checking that the new URL is correct
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Use -- I CLICK "SPECIFIC-TEXT" -- for clicking a hyperlink with the correct text in it.
		When I click "issue a call-up"
# Use -- I SHOULD BE ON "SPECIFIC-URL" -- to check the URL alias of the current page.
		Then I should be on "/issue-call-up"
		And I should see "Issue a call-up" in the "h1" element

		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Use -- I CLICK THE "SPECIFIC-TARGET" BUTTON -- to precisely target the ELEMENT and/or the CSS ID or CLASS of the SPECIFIC-TARGET button to be clicked on.
		When I click the "ul.navbar-nav.secondary" button
		Then I should be on "/user/login"
		And I should see "Username (required)"
		
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# We could also use -- I CLICK ON THE ELEMENT WITH CSS SELECTOR "SPECIFIC-CSS" -- for clicking anything that has the correct CSS on it.
		When I click on the element with css selector ".navbar-nav.secondary"
		Then I should be on "/user/login"
		And I should see "Username (required)"
# We could also use -- THE URI SHOULD MATCH -- or -- THE URL SHOULD MATCH -- but those require the full HTTP// path, and are harder to use when dev environments vary.


# more advanced clicking functions 1):
                Then I click the :arg1 element with :arg2 attribute containing :arg3
                Then I click the "input" element with "value" attribute containing "Yes Approve button"

# more advanced clicking functions 2):
                Then I click the parent element of :arg1 element containing :arg2
                Then I click the parent element of "#approval-button a" element containing "Batman is back"

# more advanced clicking functions 3):
                Then I click parent "td" element of first element containing "Batman is back"
                Then I click parent element of element containing "Batman is back"

	Scenario: Changing languages while testing. These steps change the domain name of the site from EN to FR and back again. 
# All scenarios begin in English by default, EN URLs will work fine.
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Use -- I RUN THE TEST IN FRENCH or I RUN THE TEST IN ENGLISH --.
		When I run the test in French
		And I am on "/mes-accords"
		Then I should see "Mes accords" in the "h1" element
		When I run the test in English
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Also can use just one word -- Use FRENCH or ENGLISH or FRANCAIS or ANGLAIS --. 
		When French
		Given I am on "/mes-accords"
		Then I should see "Mes accords" in the "h1" element
		When English
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
		Given Francais
		Given I am on "/mes-accords"
		Then I should see "Mes accords" in the "h1" element
		Given Anglais
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Note - CaPS DOn'T MATTeR.
		When anglais
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
		When francais
		Given I am on "/mes-accords"
		Then I should see "Mes accords" in the "h1" element

	Scenario: Pausing the test for X number of seconds, this can be helpful when the site needs to finish loading something that is about to be tested. 
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
		When I click "issue a call-up"
# Use -- I PAUSE X SECONDS -- to pause the test for a set number of seconds.
		And I pause 2 seconds
		Then I should see "Issue a call-up" in the "h1" element

    Scenario: Selecting an item from a drop list.
		Given I am on "/my-agreements"
		Then I should see "10" in the "#edit-items-per-page" element
		And I should see "25" in the "#edit-items-per-page" element
		Then I should see "Showing 1 to 10"
# Use -- I SELECT OPTION "SPECIFIC-LIST-ITEM" FROM "SPECIFIC-LIST" -- to pick an item from the list.
		When I select option "25" from "items_per_page"
		And I click the "#edit-submit-wetkit-search-api" button
		Then I should see "Showing 1 to 25"
				
	Scenario: Using the browser BACK and FORWARD buttons. 
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
		When I click the "li#wb-lng" button
		And I run the test in French
		Then I should be on "/mes-accords"
		And I should see "Mes accords" in the "h1" element
# Use -- WHEN I GO BACK -- to activate the browser Back history button.
		When I go back
		Then I should be on "/my-agreements"
		And I should see "My Agreements" in the "h1" element
# Use -- WHEN I GO FORWARD -- to activate the browser Forward history button.
		When I go forward
		Then I should be on "/mes-accords"
		And I should see "Mes accords" in the "h1" element
# Note can also use I NAVIGATE BACK ONE PAGE and I NAVIGATE FORWARD ONE PAGE.

	Scenario: Check that an ELEMENT does or does not exist. 
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Use -- I SHOULD SEE A "SPECIFIC-ELEMENT" ELEMENT -- to confirm there is at least one of that element on the page.
		And I should see a "body" element
# Use -- I SHOULD NOT SEE A "SPECIFIC-ELEMENT" ELEMENT -- to confirm there are none of that element on the page.
		But I should not see a "figure" element
# Use -- I SHOULD SEE # "SPECIFIC-ELEMENT" ELEMENTS -- to confirm the correct total number of that element exists on the page.
		And I should see 1 "body" elements

	Scenario: Check that an ELEMENT does or does not have the specified HTML somewhere inside. 
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Use -- THE "SPECIFIC-TARGET" ELEMENT SHOULD CONTAIN "SPECIFIC-HTML" -- to confirm that element has the specified HTML.
		Then the "li.menu-mlid-2230" element should contain "wb-navcurr"
# Note when additional quotes need to be added INSIDE the Behat double quotes put a \slash\ in front of them to \"Escape\" those extra quotes.
		And the "body" element should contain "id=\"wb-tphp\""
# Use -- THE "SPECIFIC-ELEMENT" ELEMENT SHOULD NOT CONTAIN "SPECIFIC-HTML"  -- to confirm that element does not have the specified CSS.
		But the "li.menu-mlid-2156" element should not contain "wb-navcurr"

	Scenario: Using the website language switcher to change languages to verify that a page has a translation in place and the URLs for both are as they should be. 
		Given I am on "/my-agreements"
		Then I should see "My Agreements" in the "h1" element
# Using the ID of the language switcher button in the top right corner to go to the translation of that page ... li#wb-lng.
		When I click the "li#wb-lng" button
		And I run the test in French
		Then I should be on "/mes-accords"
		And I should see "Mes accords" in the "h1" element
		When I click the "li#wb-lng" button
		And I run the test in English
		Then I should be on "/my-agreements"
		And I should see "My Agreements" in the "h1" element

	Scenario: Using the browser REFRESH PAGE button. This could be useful when you need to clear and reset things to original state.
		Given I am on "/user/login"
		Then I should see "Username (required)"
# Use -- WHEN I RELOAD THE PAGE -- to use the browser REFRESH PAGE option.
		When I reload the page
		And I pause 5 seconds
		Then I should see "Username (required)"

  Scenario: Single command login method for simple catalog (requires a valid username and password) at /tma
        When I log into simple catalog with username "bogus.user@my-catalogus.example.com" password "testpwd"
        # DO STUFF
        # THEN LOGOUT
        Then I log out if we see "Sign out" in the "ul.navbar-nav.secondary li.last a" element
        Then I navigate to "/my-workspace"

  Scenario: Single command login method for tma (requires a valid username and password) at /tma
        When I log into tma with username "bogus.user@tma.example.com" password "testpwd"
        # DO STUFF
        # THEN LOGOUT
        Then I navigate to "/user/logout"

  Scenario: Single command login method for pdata (requires a valid username and password) at /procurement-data/user
        When I log into pdata with username "bogus.user@pdata.example.com" password "testpwd"
        # DO STUFF
        # THEN LOGOUT
        Then I navigate to "/procurement-data/user/logout"

  Scenario: Hover over an element
        When I hover over the element "id|name|class|something"

# NOTE: this is for sosaa single command login, for pdata logins use the other login scenario above this one
  Scenario: Single command login method for sosaa (requires a valid username and password)
        When I log into sosaa with username "q-ca@eton.ca" password "test"
        # DO STUFF
        # THEN LOGOUT
        Then I log out if we see "Sign out" in the "ul.navbar-nav.secondary li.last a" element

# NOTE: this is for sri single command login, for other apps logins use their login command
  Scenario: Single command login method for sri (requires a valid username and password)
        When I log into sri with username "q-ca@eton.ca" password "test"
        # DO STUFF
        # THEN LOGOUT
        Then I log out if we see "Sign out" in the "ul.navbar-nav.secondary li.last a" element

	Scenario: Filling in one or more Input fields. This example performs a login but uses Drupal login and not the Accounts login method seen on Production.
		Given I am on "/user/login"
		Then I should see "Username (required)"
# Use -- WHEN I FILL IN "FIELD-ID" with "INPUT-VALUE" -- for singular inputs
		When I fill in "edit-name" with "q-ca@eton.ca"
		And I fill in "edit-pass" with "test"
		And I click the "#edit-submit" button
                # OR
                #And I press "#edit-submit"
		Then I should be on "/my-workspace"
		And I should see "Welcome"
		And I should see "q-ca test"
		Given I am on "/user/logout"
		Then I should see "Sign in"

# SCENARIO - CKEDITOR FIELDS, ckeditor expects us to use its API.  See examples below:
#EXAMPLE:   Then I fill in wysiwyg on field :locator with :value
# example below, notice no pound symbol before the locator (form id).
            Then I fill in wysiwyg on field "edit-field-description-en-und-0-value" with "Just a rehearsal"
            Then I fill in wysiwyg on field "edit-field-description-fr-und-0-value" with "Juste une répétition"
#This function was found here: https://gist.github.com/johnennewdeeson/240e2b60c23ea3217887


# Use -- THEN I FILL IN THE FOLLOWING -- for multiple inputs, add Field IDs followed by Input Values separated by | PIPES | AND SPACES |
		Given I am on "/user/login"
		Then I should see "Username (required)"
		Then I fill in the following:
		    | edit-name | q-ca@eton.ca |
		    | edit-pass | test |
		And I click the "#edit-submit" button
                # OR
                #And I press "#edit-submit"
		Then I should be on "/my-workspace"
		And I should see "Welcome"
		And I should see "q-ca test"
		Given I am on "/user/logout"
		Then I should see "Sign in"

	Scenario: Verifying that data entered into input fields is or is not the correct vaule.
		Given I am on "/user/login"
		Then I should see "Username (required)"
		Then I fill in the following:
		    | edit-name | q-ca@eton.ca |
		    | edit-pass | test |
		And I click the "#edit-submit" button
                # OR
                #And I press "#edit-submit"
		Then I should be on "/my-workspace"
		When I fill in "edit-title-field-value" with "auto"
		And I click the "#edit-submit-wetkit-og-all-user-group-content" button
# Use -- THEN THE "TARGET-FIELD" SHOULD CONTAIN "SPECIFIC-CONTENT" -- to confirm if expected value is in the field
        Then the "edit-title-field-value" field should contain "auto"
# Use -- THEN THE "TARGET-FIELD" SHOULD NOT CONTAIN "SPECIFIC-CONTENT" -- to confirm that the expected value is not in the field
        And the "edit-title-field-value" field should not contain ""

	Scenario: Selecting a single option from a drop-list of choices. This does not apply to checking boxes from a list checkbox choices.
		Given I am on "/my-agreements"
		Then I should see "10" in the "#edit-items-per-page" element
		And I should see "25" in the "#edit-items-per-page" element
		Then I should see "Showing 1 to 10"
# Use -- WHEN I SELECT OPTION "VALUE" FROM "LIST-ID" -- to pick the first choice from the list.
		When I select option "25" from "items_per_page"
		And I click the "#edit-submit-wetkit-search-api" button
                # OR
                #And I press "#edit-submit-wetkit-search-api"
		Then I should see "Showing 1 to 25"

	Scenario: Selecting a multiple options from a drop-list of choices. This does not apply to checking boxes from a list checkbox choices.
		Given I am on "/user/login"
		Then I should see "Username (required)"
		Then I fill in the following:
		    | edit-name | q-ca@eton.ca |
		    | edit-pass | test |
		And I click the "#edit-submit" button
		Then I should be on "/my-workspace"
# Use -- WHEN I SELECT OPTION "VALUE" FROM "LIST-ID" -- to pick the first choice from the multi-select list.
		When I select option "page" from "edit-combine-select-type"
# Use -- WHEN I ADDITIONALLY SELECT OPTION "VALUE" FROM "LIST-ID" -- to pick the additional choices from the multi-select list.
		And I additionally select "alert" from "edit-combine-select-type"
		And I click the "#edit-submit-wetkit-og-all-user-group-content" button
		Then I should see "Page" in the "table" element
		And I should see "Alert" in the "table" element
		But  I should not see "Introduction" in the "table" element

	Scenario: Selecting a multiple options from a drop-list of choices. This does not apply to checking boxes from a list checkbox choices.
		Given I am on "/user/login"
		Then I should see "Username (required)"
		Then I fill in the following:
		    | edit-name | q-ca@eton.ca |
		    | edit-pass | test |
		And I click the "#edit-submit" button
		Then I should be on "/my-workspace"

#NOTE: RATHER THAN USE MULTISTEP LOGIN, it is best to handle the login through a custom command:

# NOTE: this is for simple catalog single command login, for other apps logins use their login command
  Scenario: Single command login method for simple catalog (requires a valid username and password)
        When I log into simple catalog with username "q-ca@eton.ca" password "test"
        # DO STUFF
        # THEN LOGOUT
        Then I log out if we see "Sign out" in the "ul.navbar-nav.secondary li.last a" element

# Use -- WHEN I CHECK "CHECKBOX-TARGET" -- to add a check to the checkbox.
		When I check "Select all"
# Use -- THEN THE "CHECKBOX-TARGET" CHECKBOX SHOULD BE CHECKED -- to verify the checkbox is actually checked.
		Then the "Select all" checkbox should be checked
# Use -- WHEN I UNCHECK "CHECKBOX-TARGET" -- to remove a check from the checkbox.
		When I uncheck "Select all"
# Use -- THEN THE "CHECKBOX-TARGET" CHECKBOX SHOULD BE UNCHECKED -- to verify the checkbox is actually unchecked.
		Then the "Select all" checkbox should be unchecked

	Scenario: Verifying the response codes returned by the server after a given action.
		Given I am on "/user/login"
		Then I should see "Username (required)"
		Given I go to "/my-workspace"
# Use -- THEN THE RESPONSE CODE SHOULD BE ### -- to confirm the response code matches expectations.
		Then the response status code should be 403
# Use -- THEN THE RESPONSE CODE SHOULD NOT BE ### -- to confirm the response code matches is an incorrect one.
		And the response status code should be 200		

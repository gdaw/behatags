Feature: Create new Landing Page content and publish content - role Creator

  Scenario: Logged in users should be able to create and manage content appropriate to their roles - role Creator

  # Creator logs into Agrisource
    Given I am on "/user/logout"
    Then I pause 2 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | agrisourcebehat_creator |
          | edit-pass | PasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
  
  # Creator role creates a new Landing Page  
    # Verify that we are on the Create Landing Page form and that an empty form submission generates validation errors
    Given I am on "/node/add/landing_page"
    Then I should see "Create Landing Page" in the "h1" element
    When I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "Title (English) field is required."
    And I should see "Title (French) field is required."

    # Add input or make selections for each field
        # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in "edit-title-0-value" with "Test Landing Page title EN by agrisourcebehat_creator"   
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field
    And I fill in "edit-title-etuf-fr-0-value" with "Test Landing Page title FR by agrisourcebehat_creator"
    
    Then I fill in the following:
      | edit-revision-log-0-value | revision log Landing Page test by agrisourcecreator@yopmail.com |
      | edit-field-tracking-number-0-value | Test-tracking-number-1234 |
    
    # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 5 seconds

    # Successful Submit should display a Confirmation message and now Viewing the Landing Page
    Then I should see "has been created."
    And I should not see "Undefined variable"

    # Verify that the language switcher works to FR and back to EN and also that breadcrumbs are correct in both languages
    Then I should see "Test Landing Page title EN by agrisourcebehat_creator" in the "h1" element
    And I should see "Home" in the "nav#wb-bc" element
    And I should see "Test Landing Page title EN by agrisourcebehat_creator" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Test Landing Page title FR by agrisourcebehat_creator" in the "h1" element
    And I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Test Landing Page title FR by agrisourcebehat_creator" in the "nav#wb-bc" element
    And I should not see "Message d'erreur"

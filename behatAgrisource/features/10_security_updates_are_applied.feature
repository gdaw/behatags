Feature: Ensure that we're using the latest security advisory releases.

	Scenario: Log in, visit the reports page, verify that the correct page is loaded and then verify that the status of our core and contrib modules is secure.
    # First logout of the system, then login using admin level account
    Given I am on "/user/logout"
    Then I pause 2 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button

    # Verify security status on reports page
    Given I am on "/admin/reports/status"
    Then I should see "Drupal core update status"
    And I pause 1 seconds
    Then I should not see "Not secure!"
    

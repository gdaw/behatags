Feature: Test the content locking and unlocking functionality


  Scenario: Logged in users should lock content when editing and also unlock when saved or manually unlocked
    

    # First logout then log in with ccpub 
    Given I am on "/user/logout"
    Then I pause 4 seconds
    
    Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | agrisourcebehat_cspub |
          | edit-pass | PasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    Then I pause 12 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
    
    # Verify that we are on the Content admin page
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds

    # Test that saving content from Draft to Published does unlock the content
    Then I should see "This content is now locked against simultaneous editing"

    And I should see "Draft" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "published" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."

   # Return to /behat view to confirm content is now unlocked
    Given I am on "/admin/content/behat"
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    And I should not see "Locked by agrisourcebehat_cspub"
    
    # Return to edit to once more lock the content and test the Unlock button without saving
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
    Then I should see "This content is now locked against simultaneous editing"
    And I should see "Published" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I click the "#edit-unlock" button
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "Lock broken. Anyone can now edit this content."
    And I should not see "Error message"

   # Return to /behat view to confirm content is now unlocked
    Given I am on "/admin/content/behat"
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    And I should not see "Locked by agrisourcebehat_cspub"

    # Reset this content back to Draft, via Archived to remove the Published revision as well
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
    Then I should see "This content is now locked against simultaneous editing"

    And I should see "Published" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "archived" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
    
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
    Then I should see "This content is now locked against simultaneous editing"

    And I should see "Archived" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm revive alert
    And I pause 1 seconds
    Then I select option "draft" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
    And I should not see "Error message"

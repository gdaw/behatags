Feature: Anonymous submits an Employment Opportunity

  Scenario: Anonymous user must be able to submit an Employment Opportunity in both EN and FR and the form validation should work on each field.
  # Verify that we are on the ENGLISH Employment Opportunity submit form and that an empty form submission generates validation errors
    When I am on "/user/logout"
    Then I am on "/human-resources/staffing/internal-employment-opportunity-request-form"
    And I pause 2 seconds
    Then I should see "Internal Employment Opportunity Request Form" in the "h1#wb-cont" element
    When I click the "#edit-submit" button
    Then I pause 2 seconds
    Then I should see "Email address of Human Resources Advisor, Staffing who approved this opportunity field is required."
    And I should see "Position Title (English) field is required."
    And I should see "Position Title (French) field is required."
#    And I should see "Employment Opportunity Type(s) field is required."
    And I should see "Branch field is required."
    And I should see "Location(s) (English) field is required."
    And I should see "Location(s) (French) field is required."
    And I should see "Open to (English) field is required."
    And I should see "Open to (French) field is required."
    And I should see "Classification Group: field is required."
#    And I should see "Date field is required."
    And I should see "Level: field is required."
    And I should see "Description (English) field is required."
    And I should see "Description (French) field is required."    
    And I should see "Name field is required."
    And I should see "Email: field is required."
    And I should see "Your name field is required."
    And I should see "Your email address field is required."
    And I should see "Your telephone number field is required."
    And I should not see "Error message"
    

  # Add input or make selections for each mandatory field
    Then I fill in the following:
      | edit-field-opportunityapproval-0-value | agsbehat-empl-tester@yopmail.com |
      | edit-title-0-value | Open Position Test Title EN (EN form) |
      | edit-title-etuf-fr-0-value | Open Position Test Title FR (EN form) |

    When I force check "Assignment"
    Then the "Assignment" checkbox should be checked
    Then I select option "Legal Services" from "edit-field-branch"

    Then I fill in the following:
      | edit-field-locations-0-value | Open Position Location EN |
      | edit-field-locations-etuf-fr-0-value | Open Position Location FR |
      | edit-field-open-to-0-value | Open Position Open To EN |
      | edit-field-open-to-etuf-fr-0-value | Open Position Open To FR |

    And I select option "AC" from "edit-field-classification-0-subform-field-class-id"
    And I select option "2" from "edit-field-classification-0-subform-field-class-level"

    Then I fill in the following:
      | edit-field-date-closing-0-value-date | 06/09/2020 |
      | edit-field-name-0-value | Test Name EN |
      | edit-field-email-0-value | agsbehat-empl-tester@yopmail.com |
      | edit-field-employmentrequestorname-0-value | Your EN name goes here |
      | edit-field-employmentrequestoremail-0-value | agsbehat-empl-tester@yopmail.com |
      | edit-field-employmentrequestorphone-0-value | ABC123 |

    # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in wysiwyg on field "edit-body-0-value" with "Test EN opportunity text."   
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field
    And I fill in wysiwyg on field "edit-body-etuf-fr-0-value" with "Test FR opportunity text."    

  # Verify each validation error is now gone
    And I pause 5 seconds
    Then I should not see "Email address of Human Resources Advisor, Staffing who approved this opportunity field is required."
# @todo, fix the twig template for these fields so it works, or if it's not that then check for custom js, James worked on this.
#    And I should not see "Position Title (English) field is required."
#    And I should not see "Position Title (French) field is required."
#    And I should not see "Employment Opportunity Type(s) field is required."
    And I should not see "Branch field is required."
    And I should not see "Location(s) (English) field is required."
    And I should not see "Location(s) (French) field is required."
#    And I should not see "Open to (English) field is required."
#    And I should not see "Open to (French) field is required."
    And I should not see "Classification Group: field is required."
    And I should not see "Translation request number field is required."
    And I should not see "Date field is required."
#    And I should not see "Description (English) field is required."
#    And I should not see "Description (French) field is required."
    And I should not see "Name field is required."
#    And I should not see "Email: field is required."
    And I should not see "Your name field is required."
    And I should not see "Your email address field is required."
    And I should not see "Your telephone number field is required."
    And I should not see "Error message"

  # TODO - Only mandatory fields are validated, non-mandatory fields and not tested currently
    # Add Classification
    # Add Sub Group
    # Notes

  # Verify functionality of the button ... Preview
    When I click the "#edit-preview" button
    And I pause 5 seconds
    Then I should see "Back to content editing"
    And I should see "agsbehat-empl-tester@yopmail.com"
    And I should see "Open Position Location EN"
    
  # Verify functionality of the link ... Return to form in progress
    When I click the "#edit-backlink" button
    And I pause 5 seconds
    Then I should not see "Back to content editing"
    
  # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 5 seconds

  # Successful Submit should display a Confirmation message
    And I should see "Employment Opportunity"
    Then I should see "has been created."
    And I should not see "Undefined variable:"
  # Successful Submit should auto-forward to the Home Page
    Then I should see "Find a colleague"

  # Verify that the language switcher works from EN News submit form to FR and back and that breadcrumbs are correct in both languages
    Given I am on "/human-resources/staffing/internal-employment-opportunity-request-form"
    Then I should see "Home" in the "nav#wb-bc" element
    And I should see "Human Resources" in the "nav#wb-bc" element
    And I should see "Staffing" in the "nav#wb-bc" element
    And I should see "Internal Employment Opportunity Request Form" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Formulaire de demande pour les possibilités d'emploi à l'interne" in the "h1#wb-cont" element
    Then I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Ressources humaines" in the "nav#wb-bc" element
    And I should see "Dotation" in the "nav#wb-bc" element
    And I should see "Formulaire de demande pour les possibilités d'emploi à l'interne" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Internal Employment Opportunity Request Form" in the "h1#wb-cont" element
    And I should not see "Error message"

 # Verify that we are on the Français Employment Opportunity submit form and that an empty form submission generates validation errors     
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Formulaire de demande pour les possibilités d'emploi à l'interne" in the "h1#wb-cont" element
    When I click the "#edit-submit" button
    And I pause 2 seconds
    Then I should see "Adresse courriel du conseiller en ressources humaines responsable de la dotation qui a approuvé cette possibilité field est requis."
    And I should see "Titre du poste (Français) field est requis."
    And I should see "Titre du poste (Anglais) field est requis."
    And I should see "Type de possibilité d'emploi est requis."
    And I should see "Direction générale field est requis."
    And I should see "Lieu(x) (Français) field est requis."
    And I should see "Lieu(x) (Anglais) field est requis."
    And I should see "Ouvert à (Français) field est requis."
    And I should see "Ouvert à (Anglais) field est requis."
    And I should see "Groupe de classification : field est requis."
    And I should see "Niveau : field est requis."
    And I should see "Date field est requis."
    And I should see "Description (Français) field est requis."
    And I should see "Description (Anglais) field est requis."    
    And I should see "Nom field est requis."
    And I should see "Courriel : field est requis."
    And I should see "Votre nom au complet field est requis."
    And I should see "Votre adresse de courriel field est requis."
    And I should see "Votre numéro de téléphone field est requis."
    And I should not see "Message d'erreur"

  # Add input or make selections for each mandatory field
    When I fill in the following:
      | edit-field-opportunityapproval-0-value | agsbehat-empl-tester@yopmail.com |
      | edit-title-0-value | Open Position Test Title FR (FR form) |
      | edit-title-etuf-en-0-value | Open Position Test Title EN (FR form) |

    Then I force check "Affectation"
    Then the "Affectation" checkbox should be checked
    Then I select option "Services juridiques" from "edit-field-branch"

    Then I fill in the following:
      | edit-field-locations-0-value | Open Position Location FR |
      | edit-field-locations-etuf-en-0-value | Open Position Location EN |
      | edit-field-open-to-0-value | Open Position Open To FR |
      | edit-field-open-to-etuf-en-0-value | Open Position Open To EN |

    And I select option "AC" from "edit-field-classification-0-subform-field-class-id"
    And I select option "2" from "edit-field-classification-0-subform-field-class-level"

    Then I fill in the following:
      | edit-field-date-closing-0-value-date | 06/09/2020 |
      | edit-field-name-0-value | Test Name FR |
      | edit-field-email-0-value | agsbehat-empl-tester@yopmail.com |
      | edit-field-employmentrequestorname-0-value | Your FR name goes here |
      | edit-field-employmentrequestoremail-0-value | agsbehat-empl-tester@yopmail.com |
      | edit-field-employmentrequestorphone-0-value | ABC123 |

    # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in wysiwyg on field "edit-body-0-value" with "Test FR opportunity text."   
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-en-0-value" field
    And I fill in wysiwyg on field "edit-body-etuf-en-0-value" with "Test EN opportunity text."    

  # Verify each validation error is now gone
    And I pause 3 seconds
    Then I should not see "Adresse courriel du conseiller en ressources humaines responsable de la dotation qui a approuvé cette possibilité field est requis."
#    And I should not see "Titre du poste (Français) field est requis."
#    And I should not see "Titre du poste (Anglais) field est requis."
#    And I should not see "Type de possibilité d'emploi field est requis."
    And I should not see "Direction générale field est requis."
    And I should not see "Lieu(x) (Français) field est requis."
    And I should not see "Lieu(x) (Anglais) field est requis."
#    And I should not see "Ouvert à (Français) field est requis."
#    And I should not see "Ouvert à (Anglais) field est requis."
    And I should not see "Groupe de classification : field est requis."
    And I should not see "Niveau : field est requis."
    And I should not see "Date field est requis."
#    And I should not see "Description (Français) field est requis."
#    And I should not see "Description (Anglais) field est requis."    
    And I should not see "Nom field est requis."
#   And I should not see "Courriel : field est requis."
    And I should not see "Votre nom au complet field est requis."
    And I should not see "Votre adresse de courriel field est requis."
    And I should not see "Votre numéro de téléphone field est requis."
    And I should not see "Message d'erreur"

  # TODO - Only mandatory fields are validated, non-mandatory fields and not tested currently
    # Add Classification
    # Add Sub Group
    # Notes

  # Verify functionality of the button ... Preview
    When I click the "button#edit-preview" button
    And I pause 5 seconds
    Then I should see "Revenir à la modification du contenu"
    And I should see "agsbehat-empl-tester@yopmail.com"
    And I should see "Open Position Location FR"
    
  # Verify functionality of the link ... Return to form in progress
    When I click the "#edit-backlink" button
    And I pause 5 seconds
    And I should not see "Revenir à la modification du contenu"
  
  # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 8 seconds

  # Successful Submit should display a Confirmation message
    And I should see "a été créé"
  # Successful Submit should auto-forward to the Home Page
    Then I should see "Trouver un collègue"
    
 # Clean up post-test - delete the content created by this test
 #  Given I am on "/user/login"
 #  Then I should see "Log in"
 #  When I fill in "edit-name" with "agrisourcebehat_adm"
 #  And I fill in "edit-pass" with "PasswordTestLongMotDePasse1"
 #  And I click the "#edit-submit" button
 #  Then I should see "Moderation Dashboard" in the "h1" element
 #  Given I am on "/admin/content/behat"
 #  And I pause 2 seconds
 #  And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
 #  And I click the "#edit-submit-content" button
 #  And I pause 5 seconds
 #  Then I should see "Open Position Test Title EN (EN form) delete link"
 #  When I click "Open Position Test Title EN (EN form) delete link"
 #  And I pause 5 seconds
 #  Then I click the "#edit-submit" button
 #  Then I should see "Open Position Test Title EN (EN form) has been deleted"
 #  And I fill in "edit-title" with "Open Position Test Title EN (FR form)"
 #  And I click the "#edit-submit-content" button
 #  And I pause 5 seconds
 #  Then I should see "Open Position Test Title EN (FR form) delete link"
 #  When I click "Open Position Test Title EN (FR form) delete link"
 #  And I pause 5 seconds
 #  Then I click the "#edit-submit" button
 #  Then I should see "Open Position Test Title EN (FR form) has been deleted"
   

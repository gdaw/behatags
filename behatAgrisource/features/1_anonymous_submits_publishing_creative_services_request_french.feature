#/branches-and-offices/public-affairs-branch/publishing-and-creative-services-request-form
Feature: Anonymous submits a Publishing and Creative Services Request

  Scenario: Anonymous user must be able to submit aa Publishing and Creative Services Request in both EN and FR and the form validation should work on each field.
  # Verify that the language switcher works from EN to FR and back and that breadcrumbs are correct in both languages
    Given I am on "/branches-and-offices/public-affairs-branch/publishing-and-creative-services-request-form"
    Then I should see "Home" in the "nav#wb-bc" element
    And I should see "Branches and Offices" in the "nav#wb-bc" element
    And I should see "Public Affairs Branch" in the "nav#wb-bc" element
    And I should see "Publishing and Creative Services Request Form" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Formulaire de demande des services d’édition" in the "h1#wb-cont" element
    Then I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Directions générales et bureaux" in the "nav#wb-bc" element
    And I should see "Direction générale des affaires publiques" in the "nav#wb-bc" element
    And I should see "Formulaire de demande des services d’édition" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 4 seconds
    Then I should see "Publishing and Creative Services Request Form" in the "h1#wb-cont" element

 # Verify that we are on the FRENCH form and that an empty form submission generates validation errors     
    When I click the "a.language-link" button
    And I pause 4 seconds
    Then I should see "Formulaire de demande des services d’édition" in the "h1#wb-cont" element
    When I click the "#edit-actions-submit" button
    And I pause 2 seconds
    Then I should see "Nom - Ce champ est obligatoire."
    And I should see "Courriel - Ce champ est obligatoire."
    And I should see "Direction générale - Ce champ est obligatoire."
    And I should see "S’agit-il d’une nouvelle demande ou d’une mise à jour d’un projet existant? - Ce champ est obligatoire."
    And I should see "Titre du projet - Ce champ est obligatoire."
    And I should see "Date d’échéance - Ce champ est obligatoire."
    And I should see "Cette demande est-elle liée à une activité planifiée? - Ce champ est obligatoire."
    And I should see "Veuillez indiquer quelle est la priorité ministérielle qu’appuie votre demande de publication - Ce champ est obligatoire."
    And I should see "Quel est le message principal que vous souhaitez communiquer? - Ce champ est obligatoire."
    And I should see "Public cible - Ce champ est obligatoire."
    And I should see "Votre conseiller en communication a-t-il approuvé ce projet? - Ce champ est obligatoire."
    And I should see "Produit - Ce champ est obligatoire."
    And I should see "Le contenu a-t-il été approuvé ? - Ce champ est obligatoire."
    And I should see "Le contenu est-il final en anglais et en français? - Ce champ est obligatoire."
#Error shows up if email not configured correctly.
#    And I should not see "Message d'erreur"
    
    # Add input or make selections for each mandatory field
    Then I fill in the following:
      | edit-srf-name | FR name goes here |
      | edit-srf-phone | +1 514-222-2222 |
      | edit-srf-email | agsbehat-publishingCreativeServicesRequest-tester@yopmail.com |

    Then I select option "Direction générale des sciences et de la technologie" from "edit-srf-branch"
    Then I select option "Non" from "edit-is-your-project-submission-related-to-any-other-request-already-"
    Then I select option "Non" from "edit-srf-contentfinalizedinenglishandfrench"
    Then I select option "Nouvelle" from "edit-srf-isnewrequestorexistingproject"
    
    Then I fill in the following:
      | edit-rsf-projecttitle | FR Project Title goes here |
      | edit-rsf-daterequired | 2029-01-01 |

    Then I select option "Non" from "edit-srf-isrequesttoscheduledevent"

    Then I fill in the following:
      | edit-srf-departmentalpriority | FR departmental priority goes here |
      | edit-srf-mainmessage | FR main message goes here |

    Then I select option "Consommateurs" from "edit-srf-targetaudience"
    Then I select option "Non" from "edit-srf-hascommadvisorapproved"
    Then I select option "DG" from "edit-level-of-approval-required"
    #Then I select option "Carte" from "edit-srf-product"
    And I check "Carte"
    Then I select option "Non" from "edit-srf-hascontentpproved"
    #############Then I select option "Non" from "edit-srf-contentfinalizedinenglishandfrench"

      # Verify each validation error is now gone
    Then I click the "#edit-actions-submit" button
    And I pause 3 seconds
    Then I should not see "Le champ Nom - Ce champ est obligatoire."
    And I should not see "Le champ Courriel - Ce champ est obligatoire."
    And I should not see "Le champ Direction générale - Ce champ est obligatoire."
    And I should not see "Le champ S’agit-il d’une nouvelle demande ou d’une mise à jour d’un projet existant? - Ce champ est obligatoire."
    And I should not see "Le champ Titre du projet - Ce champ est obligatoire."
    And I should not see "Le champ Date d’échéance - Ce champ est obligatoire."
    And I should not see "Le champ Cette demande est-elle liée à une activité planifiée? - Ce champ est obligatoire."
    And I should not see "Le champ Veuillez indiquer quelle est la priorité ministérielle qu’appuie votre demande de publication - Ce champ est obligatoire."
    And I should not see "Le champ Quel est le message principal que vous souhaitez communiquer? - Ce champ est obligatoire."
    And I should not see "Le champ Public cible - Ce champ est obligatoire."
    And I should not see "Le champ Votre conseiller en communication a-t-il approuvé ce projet? - Ce champ est obligatoire."
    And I should not see "Le champ Produit - Ce champ est obligatoire."
    And I should not see "Le champ Le contenu a-t-il été approuvé ? - Ce champ est obligatoire."
    #And I should not see "Message d'erreur"
    #############And I should not see "Le contenu est-il final en anglais et en français? - Ce champ est obligatoire."

  # Verify functionality of the button ... Submit
    #When I click the "#edit-actions-submit" button
    #And I pause 5 seconds

  # Successful Submit should display a Confirmation message
    And I should not see "erreurs ont été trouvées"
    And I should see "Demande confirmée"

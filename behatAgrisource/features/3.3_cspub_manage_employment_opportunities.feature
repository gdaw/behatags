Feature: Create content and publish content - role CCPUB


  Scenario: Logged in users should be able to create and manage content appropriate to their roles - role CCPUB
    

  #CCPUB role manages News Artcle
    # First logout then log in with ccpub 
    Given I am on "/user/logout"
    Then I pause 4 seconds
    
    Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | agrisourcebehat_cspub |
          | edit-pass | PasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
    
    # Verify that we are on the Content admin page
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form)"
    When I click "Open Position Test Title EN (EN form)"
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form)" in the "h1" element
    And I should not see "Error message"

    # Verify that the language switcher works to FR and back to EN and also that breadcrumbs are correct in both languages
    Then I should see "Open Position Test Title EN (EN form)" in the "h1" element
    And I should see "Home" in the "nav#wb-bc" element
    And I should see "Open Position Test Title EN (EN form)" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Open Position Test Title FR (EN form)" in the "h1" element
    And I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Open Position Test Title FR (EN form)" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Open Position Test Title EN (EN form)" in the "h1" element
    And I should not see "Error message"

    # Verify that we are on the Content admin page
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (FR form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (FR form)"
    When I click "Open Position Test Title EN (FR form)"
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (FR form)" in the "h1" element

    # Verify that the language switcher works to FR and back to EN and also that breadcrumbs are correct in both languages
    Then I should see "Open Position Test Title EN (FR form)" in the "h1" element
    And I should not see "Message d'erreur"
    And I should see "Home" in the "nav#wb-bc" element
    And I should see "Open Position Test Title EN (FR form)" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Open Position Test Title FR (FR form)" in the "h1" element
    And I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Open Position Test Title FR (FR form)" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    #And I should see "Access unpublished hash link allows unpublished access to content" #Split into two?
    #And I should see "Access unpublished" ## FIND OUT WHY THIS ISN'T WORKING?
    #And I should see "allows unpublished access to content"
    Then I should see "Open Position Test Title EN (FR form)" in the "h1" element
    # Verify moderation steps from Draft to Published to Archived and back again.
     # Return to /behat view to get back to the Edit link
    Given I am on "/admin/content/behat"
    And I pause 1 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
#    Then I should see "This content is now locked against simultaneous editing"
    
    And I should see "Draft" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "published" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."

    # Return to /behat view to get back to the Edit link
    Given I am on "/admin/content/behat"
    And I pause 1 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
    
    # Test that Published did unlock content, then test that the Edit Unlock button also works
#    Then I should see "This content is now locked against simultaneous editing"
    And I should see "Published" in the "#edit-moderation-state-0-current" element
    And I pause 1 seconds
    Then I click the "#edit-unlock" button
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "Lock broken. Anyone can now edit this content."

    # Return to /behat view to get back to the Edit link
    Given I am on "/admin/content/behat"
    And I pause 1 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
    
    And I should see "Published" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "draft" from "edit-moderation-state-0-state"
    And I pause 2 seconds
    Then I click the "#edit-submit" button
    And I pause 2 seconds
    Then I should see "has been updated."
    
    # Test to make sure the pending draft functionality works.
    Given I am on "/admin/content"
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 2 seconds
    # Make sure the pending draft functionality works.
   # Then I should see "Published + pending draft" in the "table" element
   # TODO: figure out why this step fails?
   # Then I should see "Published + pending draft"

    # Return to /behat view to get back to the Edit link
    Given I am on "/admin/content/behat"
    And I pause 1 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds

    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "archived" from "edit-moderation-state-0-state"
    And I pause 2 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."

    # Return to /behat view to get back to the Edit link
    Given I am on "/admin/content/behat"
    And I pause 1 seconds
    And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Open Position Test Title EN (EN form) edit link"
    When I click "Open Position Test Title EN (EN form) edit link"
    And I pause 1 seconds
    
    And I should see "Archived" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm revive alert
    And I pause 1 seconds
    Then I select option "draft" from "edit-moderation-state-0-state"
    And I pause 2 seconds
    Then I click the "#edit-submit" button
    And I pause 2 seconds
    Then I should see "has been updated."
    

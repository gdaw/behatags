Feature: News Bulletin Email Template Presence

  Scenario: Verify that news bulletins and email template are visible
    Given I navigate to "/news-bulletin-email"
    Then I should see "news@work"
    And I should see "nouvelles@l'ouvrage"
    And I should see "WEEKLY NEWSLETTER"
    And I should see "AAFC News Releases and Statements"
    And I should see "Communiqués et déclarations d'AAC"
    And I should see "BULLETIN HEBDOMADAIRE"
    And I should not see "Error message"

    Given I navigate to "/news-at-work-bulletin"
    Then I should see "Date modified"
    And I should see "News Bulletin Email"
    And I should see "Home"
    And I should see "new@work Bulletin - NCR only"
 #   And I should not see "Error message"
    
    Given I navigate to "/news-at-work-bulletin/outside-ncr"
    Then I should see "Date modified"
    And I should see "News Bulletin Email"
    And I should see "Home"
    And I should see "new@work Bulletin - Outside NCR"
    And I should not see "Error message"

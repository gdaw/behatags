Feature: Clean up the leftover content created during earlier behat test scenarios

Scenario: All content created during a full run of behat tests should be deleted.

    # First logout of the system, then login using admin level account
    Given I am on "/user/logout"
    And I pause 4 seconds
#    Then I should see "Find a colleague"

    Given I am on "/user/login"
    Then I should see "Log in"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button
    Then I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
    
    # Delete Internal Page(s)
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Test Internal Page title EN by agrisourcebehat_creator"
    And I click the "#edit-submit-content" button
    And I pause 5 seconds
    Then I should see "Test Internal Page title EN by agrisourcebehat_creator delete link"
    When I click "Test Internal Page title EN by agrisourcebehat_creator delete link"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    Then I should see "Test Internal Page title EN by agrisourcebehat_creator has been deleted"
    
   # Delete News Articles
   Given I am on "/admin/content/behat"
   And I pause 2 seconds
   And I fill in "edit-title" with "Test Headline EN (EN form)"
   And I click the "#edit-submit-content" button
   And I pause 5 seconds
   Then I should see "Test Headline EN (EN form) delete link"
   When I click "Test Headline EN (EN form) delete link"
   And I pause 5 seconds
   Then I click the "#edit-submit" button
   Then I should see "Test Headline EN (EN form) has been deleted"
   And I fill in "edit-title" with "Test Headline EN (FR form)"
   And I click the "#edit-submit-content" button
   And I pause 5 seconds
   Then I should see "Test Headline EN (FR form) delete link"
   When I click "Test Headline EN (FR form) delete link"
   And I pause 5 seconds
   Then I click the "#edit-submit" button
   Then I should see "Test Headline EN (FR form) has been deleted"
    
   # Delete Employment Opportunites
   Given I am on "/admin/content/behat"
   And I pause 2 seconds
   And I fill in "edit-title" with "Open Position Test Title EN (EN form)"
   And I click the "#edit-submit-content" button
   And I pause 5 seconds
   Then I should see "Open Position Test Title EN (EN form) delete link"
   When I click "Open Position Test Title EN (EN form) delete link"
   And I pause 5 seconds
   Then I click the "#edit-submit" button
   Then I should see "Open Position Test Title EN (En form) has been deleted"
   And I fill in "edit-title" with "Open Position Test Title EN (FR form)"
   And I click the "#edit-submit-content" button
   And I pause 5 seconds
   Then I should see "Open Position Test Title EN (FR form) delete link"
   When I click "Open Position Test Title EN (FR form) delete link"
   And I pause 5 seconds
   Then I click the "#edit-submit" button
   Then I should see "Open Position Test Title EN (FR form) has been deleted"
   Scenario: User accounts need to be deleted.

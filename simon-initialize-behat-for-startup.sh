#!/bin/bash

user_name=`whoami`

git config credential.helper 'store'
if grep -q "chrome" /etc/hosts;
then
    # code if found
    echo `grep -Fxq "chrome" /etc/hosts`;
else
    # code if not found
    sudo chown ${user_name}: /etc/hosts
    echo "172.17.0.2 chrome" >> /etc/hosts
    sudo chown root: /etc/hosts
    pushd /etc/apache2/sites-available;
    sudo find . -name "*.conf" -type f -exec sed -i 's/host>/host *:80>/g' {} +
    popd;
    sudo service apache2 restart;
fi

if [ ! -d "/tmp/debug-chrome" ]; then
  mkdir /tmp/debug-chrome
fi
if [ ! -d "/tmp/debug-phantomjs" ]; then
  mkdir /tmp/debug-phantomjs
fi
if [ ! -d "/tmp/behat-downloads" ]; then
  mkdir /tmp/behat-downloads
fi

echo "Log into the amazon vbox as ec2-user psw welcome1 docker services for behat now."
echo "once you are logged into THAT environment, cd /d8;"
echo "git clone https://gitlab.com/argrms/behatags.git"
echo "cd behatags;"
echo "then run the simon-start script in there."
echo ""
echo "sleeping 3 seconds so you can be reminded of the instructions above"
sleep 3

cat /etc/hosts | grep chrome
echo ""
echo "NOW VNC into 172.17.0.2 port 5900, password is 'secret'"
echo ""
echo "OK, check the behat.yml file and add in your local hostname there"
echo "OK, START BEHAT LIKE THIS:"
echo ""
echo "pushd behatAgrisource;../vendor/bin/behat --verbose -p localhostd8 features;popd;"

Agriculture intranet Portal CI PIPELINE STATUS IS BELOW! https://gitlab.com/agrcms/behatags/pipelines

Agriculture intranet portal

[![pipeline status](https://gitlab.w3usine.com/qa/behatags/badges/behat-test/pipeline.svg)](https://gitlab.w3usine.com/qa/behatags/commits/behat-test)

*MORE DOCUMENTATION*

For usage documentation see readme.md and /or the wiki here https://gitlab.w3usine.com/qa/behatExample/wikis/create-a-new-automated-test-scenario-and-run-it

*Reference documentation for behat*

Refer to this reference document for how to write a test: https://gitlab.com/agrcms/behatags/-/blob/master/behatAgrisource/features_examples/behat-steps-examples.feature.temp

BACKSTOPJS
the folder called backstopjs_ags contains configuration for backstopjs to run DWP tests, as of this writing,
the entrypoint on the docker container is incorrect so a slight tweak is needed to get that running. backstopjs might also work.
This is a very basic example configuration for using backstopjs.  I (Joseph) will followup on this asap.

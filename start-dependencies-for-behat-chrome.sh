#!/bin/bash

user_name=`whoami`

apparmor_status=`service apparmor status | grep 'Active:'| cut -f2 -d ":"`
apparmor_status=${apparmor_status%%since*}
apparmor_status=`echo $apparmor_status|xargs`
init_docker=0
if ! docker &> /dev/null; then
  echo "docker is not yet installed"
  init_docker=1
fi
if docker &> /dev/null; then
  rc=0;
  if result=$(docker ps 2>&1); then
    echo "docker service is running"
  else
    init_docker=1
  fi
fi
if [ $init_docker == 1 ]; then
    echo "docker service is not running, enable and start the docker service"
    #echo "wake up the new docker group.";
    #newgrp docker
    if [ "$(uname -a)" == *"amzn"* ]; then
      sudo apt install apt-transport-https ca-certificates curl software-properties-common
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
      sudo apt-get install docker-ce docker-compose -y;
      sudo apt update
      sudo apt-cache policy docker-ce
    else
      sudo apt-get install docker docker-compose -y;
    fi
    echo "add a docker group.";
    sudo groupadd docker;
    echo "add docker group to the current user.";
    sudo usermod -aG docker $user_name;
    echo "start docker";
    sudo service docker start;
    sudo apt autoremove -y
fi
if ! docker-compose &> /dev/null; then
  echo "installing docker-compose, it is required to run the docker-compose.yml";
  sudo apt-get install docker-compose -y;
fi
if [ "$apparmor_status" == "active (exited)" ]; then
  echo "Apparmor is currently enabled, if it is not configured it may prevent docker-compose from working correctly";
  sudo apt-get install docker-compose -y
  sudo service apparmor stop;
else
  echo "Verification checks passed."
fi

if ! grep -q "chromium" /etc/hosts;
then
    # code if found
    sudo chown ${user_name}: /etc/hosts
    echo "172.17.0.2 chromium" >> /etc/hosts
    sudo chown root: /etc/hosts
fi
if grep -q "chrome" /etc/hosts;
then
    # code if found
    echo `grep -Fxq "chrome" /etc/hosts`;
    git config credential.helper 'store'
else
    # code if not found
    sudo chown ${user_name}: /etc/hosts
    echo "172.19.0.2 chrome" >> /etc/hosts
    sudo chown root: /etc/hosts
    pushd /etc/apache2/sites-available;
    sudo find . -name "*.conf" -type f -exec sed -i 's/host>/host *:80>/g' {} +
    popd;
    sudo service apache2 restart;
fi
if [ ! -d ~/gitlab-runner ]; then
  mkdir ~/gitlab-runner;
fi

if [ ! -d "/tmp/debug-chrome" ]; then
  mkdir /tmp/debug-chrome
fi
if [ ! -d "/tmp/debug-phantomjs" ]; then
  mkdir /tmp/debug-phantomjs
fi
if [ ! -d "/tmp/behat-downloads" ]; then
  mkdir /tmp/behat-downloads
fi

echo "Starting docker services for behat now."
docker_images=`docker-compose images | wc -l`
if [ $docker_images -gt 2 ]; then
  docker-compose up -d --remove-orphans
else
  docker-compose up -d
fi

cat /etc/hosts | grep chrome
echo ""
echo "NOW VNC into 172.19.0.2 port 5900, password is 'secret'"
echo ""
echo "OK, check the behat.yml file and add in your local hostname there"
echo "OK, START BEHAT LIKE THIS:"
echo ""
echo "pushd behatAgrisource;../vendor/bin/behat --verbose -p ags9x features;popd;"
echo "pushd behatAgrisource;../vendor/bin/behat --verbose -p agrisource9x features;popd;"
